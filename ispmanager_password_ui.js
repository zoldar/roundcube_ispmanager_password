(function () {
    var R = React;
    var D = React.DOM;
    var cmd = function () {
        return window.ispmanager_password.cmd;
    };
    var utils = window.ispmanager_password.utils;
    var ui = window.ispmanager_password.components;
    var getLabel = utils.getLabel;
    var capitalize = utils.capitalize;
    var ImmutableRenderMixin = utils.ImmutableRenderMixin;

    /*
     * Components
     */

    ui._PasswordField = R.createClass({
        mixins: [ImmutableRenderMixin],

        render: function () {
            var that = this;
            return (
                D.span({className: "password-field"},
                    D.label({for: this.props.id}, capitalize(getLabel(this.props.label))),
                    D.input({
                        id: this.props.id,
                        type: 'password',
                        value: this.props.data,
                        onChange: function (e) {
                            e.preventDefault();
                            that.props.command(e.target.value);
                        }
                    })
                )
            );
        }
    });

    ui.OldPasswordField = R.createClass({
      mixins: [ImmutableRenderMixin],

      render: function () {
          return (
              R.createElement(ui._PasswordField, {
                  id: "old-password",
                  label: 'oldpassword',
                  data: this.props.data,
                  command: cmd().updateOldPassword
              })
          );
      }
    });

    ui.PasswordField = R.createClass({
        mixins: [ImmutableRenderMixin],

        render: function () {
            return (
                R.createElement(ui._PasswordField, {
                    id: "password",
                    label: 'newpassword',
                    data: this.props.data,
                    command: cmd().updatePassword
                })
            );
        }
    });

    ui.PasswordConfirmField = R.createClass({
        mixins: [ImmutableRenderMixin],

        render: function () {
            return (
                R.createElement(ui._PasswordField, {
                    id: "password-confirm",
                    label: 'newpasswordconfirm',
                    data: this.props.data,
                    command: cmd().updatePasswordConfirm
                })
            );
        }
    });

    ui.PasswordForm = R.createClass({
        mixins: [ImmutableRenderMixin],

        render: function () {
            return (
                D.div(null, [
                    D.div(null,
                        R.createElement(ui.OldPasswordField, {data: this.props.data.get('oldPassword')})
                    ),
                    D.div(null,
                        R.createElement(ui.PasswordField, {data: this.props.data.get('newPassword')})
                    ),
                    D.div(null,
                        R.createElement(ui.PasswordConfirmField, {data: this.props.data.get('newPasswordConfirm')})
                    ),
                    D.div({className: "buttons"},
                        D.input({
                            type: 'button',
                            className: 'mainaction button',
                            value: capitalize(getLabel('changepassword')),
                            onClick: function (e) {
                                e.preventDefault();
                                cmd().changePassword();
                            }
                        })
                    )
                ])
            );
        }
    });

    ui.PasswordView = R.createClass({
        mixins: [ImmutableRenderMixin],

        render: function () {
            return (
                D.div({
                        id: "filter-box",
                        className: "uibox contentbox",
                        style: {left: 0, padding: "10px", overflowY: "scroll"}
                    },
                    R.createElement(ui.PasswordForm, this.props)
                )
            );
        }
    });
})();

