"use strict";

/*
 * For localization and templating purposes. Proper JS files
 * are included separately.
 */
window.ispmanager_password = {
    labels: {},
    components: {},
    cmd: {}
};

/*
 * Common utilities used across the componenets and UI
 */
window.ispmanager_password.utils = {
    capitalize: function (string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    },
    getLabel: function (label, params) {
        params = params || {};
        var labels = window.ispmanager_password.labels;
        var output = label;

        if (labels.hasOwnProperty(label)) {
            output = labels[label];
        }

        for (var param in params) {
            if (params.hasOwnProperty(param)) {
                output = output.replace('{' + param + '}', params[param]);
            }
        }

        return output;
    },
    // Code taken from https://github.com/jurassix/react-immutable-render-mixin
    ImmutableRenderMixin: (function () {
        function shallowEqualImmutable(objA, objB) {
            if (objA === objB || Immutable.is(objA, objB)) {
                return true;
            }

            if (typeof objA !== 'object' || objA === null ||
                typeof objB !== 'object' || objB === null) {
                return false;
            }

            var keysA = Object.keys(objA);
            var keysB = Object.keys(objB);

            if (keysA.length !== keysB.length) {
                return false;
            }

            // Test for A's keys different from B.
            var bHasOwnProperty = Object.prototype.hasOwnProperty.bind(objB);
            for (var i = 0; i < keysA.length; i++) {
                if (!bHasOwnProperty(keysA[i])
                    || !Immutable.is(objA[keysA[i]], objB[keysA[i]])) {
                    return false;
                }
            }

            return true;
        }

        return {
            shouldComponentUpdate: function (nextProps, nextState) {
                return !shallowEqualImmutable(this.props, nextProps)
                    || !shallowEqualImmutable(this.state, nextState);
            }
        }
    })()
};

/*


 */
window.IspmanagerPasswordUI = function (rcube_webmail, rcmail, containerId) {
    /*
     * App state
     */
    var State = Immutable.fromJS({
        oldPassword: "",
        newPassword: "",
        newPasswordConfirm: ""
    });

    /*
     * Helpers
     */

    var utils = window.ispmanager_password.utils;

    function capitalize(string) {
        return utils.capitalize(string);
    }

    function getLabel(label, params) {
        return utils.getLabel(label, params);
    }

    /*
     * Response dispatch and handling
     */

    var Handler = {};

    Handler.passwordChangeSuccess = function (response) {
        Cmd.resetForm();
        rcmail.display_message(getLabel('passwordchanged'), "confirmation");
        window.setTimeout(
            function () {
                Cmd.logout()
            },
            2000
        );
    };

    Handler.defaultError = function (response) {
        Cmd.resetForm();
        rcmail.display_message(response.getIn(['error', 'msg', '$']), "warning");
    };

    var handlers = {};

    var successHandlers = {
        'usrparam': Handler.passwordChangeSuccess
    };

    var errorHandlers = {
        default: Handler.defaultError
    };

    // handlers dispatch entry point
    rcube_webmail.prototype.ispmanager_password_handle_response = function (_, o) {
        console.log(o);

        var isOk = o.doc.ok;
        var isError = o.doc.error;

        if (isOk) {
            successHandlers[o.doc.tparams.func.$](Immutable.fromJS(o.doc));
        } else if (isError) {
            // FIXME: better error handling TBD
            var action = 'default';
            errorHandlers[action](Immutable.fromJS(o.doc));
        } else {
            handlers[o.doc.tparams.func.$](Immutable.fromJS(o.doc));
        }
    };

    /*
     * Commands
     */

    // Low-level API call
    function apiCall(action, params) {
        params['func'] = action;

        rcmail.http_post(
            'plugin.ispmanager_password-action',
            '_params=' + encodeURIComponent(JSON.stringify(params))
        );
    }

    var Cmd = {};

    Cmd.resetForm = function () {
        State = State.set('newPassword', '').set('newPasswordConfirm', '');
        render();
    };

    Cmd.logout = function () {
        rcmail.command('switch-task', 'logout');
    };

    Cmd.updateOldPassword = function (oldPassword) {
        State = State.set('oldPassword', oldPassword);
        render();
    };

    Cmd.updatePassword = function (password) {
        State = State.set('newPassword', password);
        render();
    };

    Cmd.updatePasswordConfirm = function (passwordConfirm) {
        State = State.set('newPasswordConfirm', passwordConfirm);
        render();
    };

    function passwordsMatch(password, confirm) {
        return password === confirm;
    }

    function passwordNotEmpty(password) {
        return password;
    }

    function passwordsValid(oldPassword, password, confirm) {
        return passwordNotEmpty(oldPassword)
          && passwordNotEmpty(password)
          && passwordsMatch(password, confirm);
    }

    Cmd.changePassword = function () {
        var oldPassword = State.get('oldPassword');
        var newPassword = State.get('newPassword');
        var newPasswordConfirm = State.get('newPasswordConfirm');

        if (passwordsValid(oldPassword, newPassword, newPasswordConfirm)) {
            apiCall('usrparam', {
                sok: 'ok',
                elid: '_USER',
                old_passwd: oldPassword,
                passwd: newPassword,
                confirm: newPasswordConfirm
            });
        } else {
            var errorLabel;
            if (!passwordNotEmpty(oldPassword)) {
                errorLabel = 'oldpasswordempty';
            } else if (!passwordNotEmpty(newPassword)) {
                errorLabel = 'passwordempty';
            } else {
                errorLabel = 'passwordsdontmatch';
            }

            rcmail.display_message(getLabel(errorLabel), "warning");
        }

    };

    // Cmd is added to a public object
    // to be reachable for components
    window.ispmanager_password.cmd = Cmd;

    /*
     * Rendering function
     */
    function render() {
        var ui = window.ispmanager_password.components;

        React.render(
            React.createElement(ui.PasswordView, {data: State}),
            document.getElementById(containerId)
        );
    }

    /*
     * Public API
     */
    return {
        init: function () {
            render();
        }
    }
};

