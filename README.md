ISPManager API password plugin for Roundcube
============================================

A plugin for changing mailbox password using ISPSystem's (https://www.ispsystem.com/) ISPmanager API.

ATTENTION
---------

That plugin is still WORK IN PROGRESS and is not supposed to be used in production.


Quick start
-----------

First, fetch the plugin to Roundcube's `plugins/` folder:

    git clone git@bitbucket.org:zoldar/roundcube_ispmanager_password.git $ROUNDCUBE_HOME/plugins/ispmanager_password

Next, copy `config.inc.php.dist` to `config.inc.php` and set proper ISPManager URL.

Finally, add `ispmanager_password` to `$config['plugins']` array in the main config file under `$ROUNDCUBE_HOME/config/config.inc.php`.
    
License
-------
This plugin is released under the [GNU General Public License Version 3+](http://www.gnu.org/licenses/gpl.html).
