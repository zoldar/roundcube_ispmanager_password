<?php

class rcube_password_api
{
    private $username;
    private $password;
    private $baseUrl;
    private $debug;

    function __construct($username, $password, $baseUrl, $debug = false)
    {
        $this->username = $username;
        $this->password = $password;
        $this->baseUrl = $baseUrl;
        $this->debug = $debug;
    }

    function query($params)
    {
        $queryParams = array();
        foreach ($params as $field => $param) {
            $queryParams[] = urlencode($field)
                . '=' . urlencode(str_replace('_USER', $this->username, $param));
        }

        $queryStr = implode('&', $queryParams);

        $fullUrl = $this->baseUrl . "?authinfo="
            . $this->username . ":" . $this->password
            . "&out=json&" . $queryStr;

        if ($this->debug) {
            rcube::write_log("ispmanager_password", 'Sending API request: '.$fullUrl);
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fullUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);

        if ($this->debug) {
            rcube::write_log("ispmanager_password", 'Recevied API response: '.$response);
        }

        return json_decode($response, true);
    }
}
