window.ispmanager_password.labels['passwordtitle'] = 'Zmiana hasła';
window.ispmanager_password.labels['oldpassword'] = 'Podaj stare hasło';
window.ispmanager_password.labels['newpassword'] = 'Podaj nowe hasło';
window.ispmanager_password.labels['newpasswordconfirm'] = 'Nowe hasło ponownie';
window.ispmanager_password.labels['changepassword'] = 'Zmień hasło';
window.ispmanager_password.labels['passwordchanged'] = 'Hasło zostało zmienione';
window.ispmanager_password.labels['passwordsdontmatch'] = 'Hasła nie są takie same';
window.ispmanager_password.labels['passwordempty'] = 'Nie podano hasła';
window.ispmanager_password.labels['oldpasswordempty'] = 'Nie podano starego hasła';
