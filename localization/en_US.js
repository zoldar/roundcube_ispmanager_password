window.ispmanager_password.labels['passwordtitle'] = 'Password change';
window.ispmanager_password.labels['oldpassword'] = 'Old password';
window.ispmanager_password.labels['newpassword'] = 'New password';
window.ispmanager_password.labels['newpasswordconfirm'] = 'Confirm password';
window.ispmanager_password.labels['changepassword'] = 'Change password';
window.ispmanager_password.labels['passwordchanged'] = 'Password changed';
window.ispmanager_password.labels['passwordsdontmatch'] = 'Passwords don\'t match';
window.ispmanager_password.labels['passwordempty'] = 'Password is empty';
window.ispmanager_password.labels['oldpasswordempty'] = 'Old password is empty';
